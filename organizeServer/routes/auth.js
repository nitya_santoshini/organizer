const express = require('express');
const passport = require('passport');
const User = require('../models/User');

const router = express.Router();

// Importing Controllers
const authController = require('../controllers/authController');

// Google Authentication
router.get('/google',
  passport.authenticate('google', { scope: ['profile'] })
);

router.get('/google/home', function(req, res) {
  res.render('home');
});

// Login Route
router.get('/login', (req, res) => res.render('login'));

// Register Route
router.get('/register', (req, res) => res.render('register'));

// Register POST Handle
router.post('/register', authController.registerHandle);

// Login POST Handle
router.post('/login', authController.loginHandle);

// Logout GET Handle
router.get('/logout', authController.logoutHandle);

module.exports = router;
