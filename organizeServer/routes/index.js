const express = require('express');
const bodyParser = require("body-parser");

const router = express.Router();

/* GET home page. */
router.route('/')
.get(function(req, res, next) {
  res.render('index');
});

module.exports = router;
