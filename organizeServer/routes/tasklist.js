const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');

const Tasklist = require('../models/tasklists');
const Task = require('../models/tasks');

const router = express.Router();

// Importing Controllers
const taskController = require('../controllers/taskController');

// Tasklists main route
router.get('/', taskController.allTasklistsGetHandle);

// Tasklist route
router.get('/:tasklistId', taskController.tasklistGetHandle);

// Task route
router.get('/:taskId', taskController.taskGetHandle);

// Tasklists main POST route
router.post('/', taskController.addTasklistHandle);

// Tasklist POST route
router.post('/:tasklistId', taskController.addTaskHandle);

// Tasklist DELETE route
router.delete('/:tasklistId', taskController.deleteTasklistHandle);


// TODO:
// Function to delete tasks.
// Put request for tasks and tasklists.

module.exports = router;
