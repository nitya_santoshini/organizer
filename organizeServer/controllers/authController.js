const passport = require('passport');
const bcryptjs = require('bcryptjs');

const User = require('../models/User');

exports.registerHandle = (req, res) => {
  User.register({ username: req.body.username }, req.body.password, function(err, user) {
    if (err) {
      console.log(err);
    } else {
      passport.authenticate('local')(req, res, function() {
        res.render('home');
      });
    }
  });
}

exports.loginHandle = (req, res) => {
  const user = new User({
    username: req.body.username,
    password: req.body.password
  });
  req.login(user, function(err){
    if (err) {
      console.log(err);
    } else {
      passport.authenticate("local")(req, res, function(){
        res.render('home');
      });
    }
  });
}

exports.logoutHandle = (req, res) => {
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/auth/login');
}
