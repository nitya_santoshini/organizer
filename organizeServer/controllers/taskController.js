const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');

const Tasklist = require('../models/tasklists');
const Task = require('../models/tasks');

function deleteTasklist(user, tasklistId) {
  for (let i = 0; i < user.tasklists.length; i++) {
    if (user.tasklists[i]._id === tasklistId) {
      user.tasklists.splice(i, 1);
      break;
    }
  }
}

exports.allTasklistsGetHandle = (req, res) => {
  if (req.isAuthenticated()) {
    res.render('tasklists/tasklistsMain', { tasklists: req.user.tasklists });
  } else {
    res.redirect('/auth/login');
  }
}

exports.addTasklistHandle = (req, res) => {
  const newTasklist = new Tasklist({
    listName: req.body.newListName
  });
  newTasklist.save();
  req.user.tasklists.push(newTasklist);
  req.user.save();
  res.redirect('/tasklist/:' + newTasklist._id);
}

exports.deleteTasklistHandle = (req, res) => {
  const tasklistId = req.params.tasklistId.substr(1, );
  Tasklist.findOne({ _id: tasklistId }, function(err, foundTasklist) {
    if (err) {
      console.log(err);
    } else {
      foundTasklist.tasks.forEach(function(task) {
        Task.deleteOne({ _id: task._id });
      });
    }
  });
  Tasklist.deleteOne({ _id: tasklistId }).then((result) => {
    console.log(result);
  });
  deleteTasklist(req.user, tasklistId);
}

exports.tasklistGetHandle = (req, res) => {
  const tasklistId = req.params.tasklistId.substr(1, );
  Tasklist.findById(tasklistId, function(err, foundTasklist) {
    if (err) {
      console.log(err);
    } else {
      console.log(foundTasklist.tasks);
      res.render('tasklists/tasklist', { tasklist: foundTasklist });
    }
  })
}

exports.addTaskHandle = (req, res) => {
  const tasklistId = req.params.tasklistId.substr(1, );
  const newTask = new Task({
    taskName: req.body.newTaskName
  });
  newTask.save();

  Tasklist.findById(tasklistId, function(err, foundTasklist) {
    if (err) {
      console.log(err);
    } else {
      foundTasklist.tasks.push(newTask);
      foundTasklist.save();
    }
  });

  res.redirect('/tasklist/:' + tasklistId);
}

exports.taskGetHandle = (req, res) => {
  const taskId = req.params.taskId.substr(1, );
  Task.findById(taskId, function(err, foundTask) {
    if (err) {
      console.log(err);
    } else {
      res.render('task', { task: foundTask });
    }
  })
}
