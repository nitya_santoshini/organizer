const mongoose = require('mongoose');

// Note Schema
const noteSchema = new mongoose.Schema({
  title: { type: String, default: 'Notes' },
  content: { type: String },
});

const Note = mongoose.model('Note', taskSchema);

module.exports = Note;
