const mongoose = require('mongoose');

// Task Schema
const taskSchema = new mongoose.Schema({
  taskName: { type: String, default: 'New Task' },
  taskDescription: { type: String, default: 'Get on it!' },
  percentCompletion: { type: Number, default: 0 }
});

const Task = mongoose.model('Task', taskSchema);

module.exports = Task;
