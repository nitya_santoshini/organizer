const mongoose = require('mongoose');

// Goal Schema
const goalSchema = new mongoose.Schema({
  title: { type: String, default: 'Goal' },
  description: { type: String },
});

const Goal = mongoose.model('Goal', goalSchema);

module.exports = Goal;
