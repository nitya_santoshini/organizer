const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const findOrCreate = require('mongoose-findorcreate');

const Tasklist = require('./tasklists');
const Goal = require('./goals');
const Journal = require('./journal');
const Note = require('./notes');

// user schema
const userSchema = new mongoose.Schema({
  email: String,
  password: String,
  googleId: String,

  tasklists: [Tasklist.schema],
  goals: [Goal.schema],
  journalEntries: [Journal.schema],
  notes: [Note.schema]
});

userSchema.plugin(passportLocalMongoose);
userSchema.plugin(findOrCreate);

const User = mongoose.model('User', userSchema);

module.exports = User;
