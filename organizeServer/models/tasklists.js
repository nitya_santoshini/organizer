const mongoose = require('mongoose');
const Task = require('./tasks');

// Tasklist schema
const tasklistSchema = new mongoose.Schema({
  listName: String,
  description: { type: String, default: 'Here are your tasks!' },
  tasks: { type: [Task.schema], required: false }
});

const Tasklist = mongoose.model('Tasklist', tasklistSchema);

module.exports = Tasklist;
