const mongoose = require('mongoose');

// Journal Schema
const journalSchema = new mongoose.Schema({
  title: { type: String, default: 'My Journal Entry' },
  date: { type: Date },
  content: { type: String }
});

const JournalEntry = mongoose.model('JournalEntry', journalSchema);

module.exports = journalEnrty;
